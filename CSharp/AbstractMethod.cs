﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp
{
    public abstract class AbstractMethod
    {
        public string Concrete()
        {
            return "I'm concrete";
        }

        public abstract void Abstract();
    }
}
