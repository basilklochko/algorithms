﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp
{
    public class ActionFunc
    {
        public Action<string> Method;
        public Func<int, string> Sub;

        public ActionFunc()
        {
            Method = new Action<string>(ActionMethod);
            Sub = new Func<int, string>(FuncMethod);
        }

        public void ActionMethod(string value)
        {
            Console.Write(value);
        }

        public string FuncMethod(int value)
        {
            return "this is " + value.ToString();
        }
    }
}
