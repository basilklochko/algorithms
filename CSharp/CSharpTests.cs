﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CSharp
{
    [TestClass]
    public class CSharpTests
    {
        [TestMethod]
        public void SealedClassExtension_Works()
        {
            var c1 = new SealedClass();
            var c2 = new SealedClass();
            Assert.AreNotEqual(c1.GetCurrentTime(), c2.GetCurrentTime());
        }

        [TestMethod]
        public void Delegate_Works()
        {
            var d = new DelegateFunc();
            d.DelegateObject("some text");
            Assert.AreEqual(d.Message, "this is some text");
        }

        [TestMethod]
        public void SameNameInterfaces_Works()
        {
            IInterface1 i1 = new  SameNameInterfaces();
            IInterface1 i2 = new SameNameInterfaces();
            Assert.AreEqual(i1.Show(), i2.Show());
        }

        [TestMethod]
        public void Singleton_Works()
        {
            var s1 = Singleton.GetInstance;
            var s2 = Singleton.GetInstance;
            Assert.AreEqual(s1.Data, s2.Data);
        }

        [TestMethod]
        public void Generics_Works()
        {
            var s1 = new Generics<string>();
            var s2 = new Generics<int>();
            Assert.AreNotEqual(s1.GetType(), s2.GetType());
        }

        [TestMethod]
        public void VirtualClass_ByBase()
        {
            VirtualClass c1 = new VirtualClassDerived();
            Assert.AreEqual(c1.GetTextNotVirtual(), "this is base (not virtual)");

            VirtualClassDerived c2 = new VirtualClassDerived();
            Assert.AreEqual(c2.GetTextNotVirtual(), "this is derivative (not virtual)");


            VirtualClass c3 = new VirtualClassDerived();
            Assert.AreEqual(c3.GetTextVirtualNotOverriden(), "this is base (virtual)");

            VirtualClassDerived c4 = new VirtualClassDerived();
            Assert.AreEqual(c4.GetTextVirtualNotOverriden(), "this is derivative (virtual)");


            VirtualClass c5 = new VirtualClassDerived();
            Assert.AreEqual(c5.GetTextVirtualOverriden(), "this is derivative (virtual)");

            VirtualClassDerived c6 = new VirtualClassDerived();
            Assert.AreEqual(c6.GetTextVirtualOverriden(), "this is derivative (virtual)");
        }

        [TestMethod]
        public void ActionFunc_Work()
        {
            var a = new ActionFunc();
            
            a.ActionMethod("test");
            Assert.IsTrue("this is 1" == a.Sub(1));
        }
    }
}
