﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp
{
    public delegate void TestDelegate(string message);

    public class DelegateFunc
    {
        public TestDelegate DelegateObject;

        public DelegateFunc()
        {
            DelegateObject = new TestDelegate(this.WriteText);            
        }

        public string Message { get; set; }

        void WriteText(string message)
        {
            Message = "this is " + message;
        }
    }
}
