﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp
{
    public class SameNameInterfaces : IInterface1, IInterface2
    {
        string IInterface1.Show()
        {
            return "show 1";
        }

        string IInterface2.Show()
        {
            return "show 2";
        }
    }

    interface IInterface1
    {        
        string Show();
    }

    interface IInterface2
    {
        string Show();
    }
}
