﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace CSharp
{
    public sealed class SealedClass
    {
        public string CurrentTime { get; set; }

        public SealedClass()
        {
            CurrentTime = DateTime.Now.Ticks.ToString();
        }
    }

    public static class SealedClassExtension
    {
        public static string GetCurrentTime(this SealedClass obj)
        {
            return obj.CurrentTime;
        }
    }
}
