﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp
{
    public class Singleton
    {
        private static Singleton theInstance = null;

        private Singleton()
        {
            Data = DateTime.Now.Ticks.ToString();
        }

        public string Data { get; set; }

        public static Singleton GetInstance
        {
            get
            {
                if (theInstance == null)
                {
                    theInstance = new Singleton();
                }

                return  theInstance;
            }
        }
    }
}
