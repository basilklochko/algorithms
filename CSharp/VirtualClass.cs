﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp
{
    public class VirtualClass
    {
        public string GetTextNotVirtual()
        {
            return "this is base (not virtual)";
        }

        public virtual string GetTextVirtualNotOverriden()
        {
            return "this is base (virtual)";
        }

        public virtual string GetTextVirtualOverriden()
        {
            return "this is base (virtual)";
        }
    }

    public class VirtualClassDerived : VirtualClass
    {
        public string GetTextNotVirtual()
        {
            return "this is derivative (not virtual)";
        }

        public string GetTextVirtualNotOverriden()
        {
            return "this is derivative (virtual)";
        }

        public override string GetTextVirtualOverriden()
        {
            return "this is derivative (virtual)";
        }
    }
}
