﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace General
{
    public partial class GeneralAlgorithms
    {
        public bool AreAnagrams(string s1, string s2)
        {
            var result = false;

            var b1 = GetByteArray(s1);
            var b2 = GetByteArray(s2);

            Array.Sort(b1);
            Array.Sort(b2);

            if (b1.Length == b2.Length)
            {
                result = true;

                for (var i = 0; i < b1.Length; i++)
                {
                    if (b1[i] != b2[i])
                    {
                        result = false;
                        break;
                    }
                }
            }

            return result;
        }

        private byte[] GetByteArray(string s)
        {
            var a = s.ToCharArray();
            var b = new List<byte>();

            for (var i = 0; i < a.Length; i++)
            {
                if (a[i] != 32)
                {
                    b.Add(Convert.ToByte(a[i]));
                }
            }

            return b.ToArray();
        }
    }
}
