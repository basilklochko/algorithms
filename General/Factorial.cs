﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace General
{
    public class Factorial
    {
        public static int Calculate(int n)
        {
            if (n == 0)
            {
                return 1;
            }

            return  n * Calculate(n - 1);
        }
    }
}
