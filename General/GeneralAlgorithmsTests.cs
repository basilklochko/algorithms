﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace General
{
    [TestClass]
    public partial class GeneralAlgorithms
    {
        [TestMethod]
        public void isStringCharachtersUniqueArray_True()
        {
            Assert.IsTrue(IsStringCharachtersUniqueArray("yes!"));
        }

        [TestMethod]
        public void isStringCharachtersUniqueArray_False()
        {
            Assert.IsFalse(IsStringCharachtersUniqueArray("yes!s"));
        }

        [TestMethod]
        public void isStringCharachtersUnique_True()
        {
            Assert.IsTrue(IsStringCharachtersUnique("yes!"));
        }

        [TestMethod]
        public void isStringCharachtersUnique_False()
        {
            Assert.IsFalse(IsStringCharachtersUnique("yes!s"));
        }

        [TestMethod]
        public void ReverseString_Works()
        {
            Assert.IsTrue(ReverseString("This is crazy, man!") == "!nam ,yzarc si sihT");
        }

        [TestMethod]
        public void RemoveDups_Dups()
        {
            Assert.IsTrue(RemoveDups("This is crazy, man!") == "This crazy,mn!"); 
        }

        [TestMethod]
        public void RemoveDups_NoDups()
        {
            Assert.IsTrue(RemoveDups("impres") == "impres");
        }

        [TestMethod]
        public void RemoveDups_AllDups()
        {
            Assert.IsTrue(RemoveDups("aaa") == "a");
        }

        [TestMethod]
        public void BubbleSort_Works()
        {
            Assert.IsTrue(string.Join(",", BubbleSort(new int[] { 1, 343, 4, 5, 2, 7, 3 })) == string.Join(",", new int[] { 1, 2, 3, 4, 5, 7, 343 }));
        }

        [TestMethod]
        public void AreAnagrams_False()
        {
            var s1 = "true";
            var s2 = "false";
            Assert.IsFalse(AreAnagrams(s1, s2));
        }

        [TestMethod]
        public void AreAnagrams_True()
        {
            var s1 = "william shakespeare";
            var s2 = "i am a weakish speller";
            Assert.IsTrue(AreAnagrams(s1, s2));
        }

        [TestMethod]
        public void ReplaceSpaces_Works()
        {
            Assert.IsTrue(ReplaceSpaces("hello world, from Basil") == "hello%20world,%20from%20Basil");
        }

        [TestMethod]
        public void CreateLinkedList_Works()
        {
            var linkedList = new LinkedList<int>();

            linkedList.Add(new LinkedListNode<int>(0));
            linkedList.Add(new LinkedListNode<int>(1));
            linkedList.Add(new LinkedListNode<int>(2));

            Assert.AreEqual(linkedList.Print(), "0, 1, 2, ");
        }

        [TestMethod]
        public void ClearLinkedList_Works()
        {
            var linkedList = new LinkedList<string>();

            linkedList.Add(new LinkedListNode<string>(0.ToString()));
            linkedList.Add(new LinkedListNode<string>(1.ToString()));
            linkedList.Add(new LinkedListNode<string>(2.ToString()));

            linkedList.Clear();

            Assert.AreEqual(linkedList.Count, 0);
        }

        [TestMethod]
        public void ReverseLinkedList_Works()
        {
            var linkedList = new LinkedList<int>();

            linkedList.Add(new LinkedListNode<int>(0));
            linkedList.Add(new LinkedListNode<int>(1));
            linkedList.Add(new LinkedListNode<int>(2));

            linkedList.Reverse();

            Assert.AreEqual(linkedList.Print(), "2, 1, 0, ");
        }

        [TestMethod]
        public void DeleteIfUNiqueLinkedList_NotHead_Works()
        {
            var linkedList = new LinkedList<int>();

            linkedList.Add(new LinkedListNode<int>(0));
            linkedList.Add(new LinkedListNode<int>(1));
            linkedList.Add(new LinkedListNode<int>(2));

            linkedList.DeleteIfUNique(1);

            Assert.AreEqual(linkedList.Print(), "0, 2, ");
        }

        [TestMethod]
        public void DeleteIfUNiqueLinkedList_Head_Works()
        {
            var linkedList = new LinkedList<int>();

            linkedList.Add(new LinkedListNode<int>(0));
            linkedList.Add(new LinkedListNode<int>(1));
            linkedList.Add(new LinkedListNode<int>(2));

            linkedList.DeleteIfUNique(0);

            Assert.AreEqual(linkedList.Print(), "1, 2, ");
        }

        [TestMethod]
        public void DeleteByIndexLinkedList_NotHead_Works()
        {
            var linkedList = new LinkedList<object>();

            linkedList.Add(new LinkedListNode<object>(0));
            linkedList.Add(new LinkedListNode<object>(1));
            linkedList.Add(new LinkedListNode<object>(2));

            linkedList.DeleteByIndex(1);

            Assert.AreEqual(linkedList.Print(), "0, 2, ");
        }

        [TestMethod]
        public void DeleteByIndexLinkedList_Head_Works()
        {
            var linkedList = new LinkedList<object>();

            linkedList.Add(new LinkedListNode<object>(0));
            linkedList.Add(new LinkedListNode<object>(1));
            linkedList.Add(new LinkedListNode<object>(2));

            linkedList.DeleteByIndex(0);

            Assert.AreEqual(linkedList.Print(), "1, 2, ");
        }

        [TestMethod]
        public void RemoveDups_Works()
        {
            var linkedList = new LinkedList<int>();

            linkedList.Add(new LinkedListNode<int>(0));
            linkedList.Add(new LinkedListNode<int>(1));
            linkedList.Add(new LinkedListNode<int>(2));
            linkedList.Add(new LinkedListNode<int>(3));
            linkedList.Add(new LinkedListNode<int>(4));
            linkedList.Add(new LinkedListNode<int>(3));
            linkedList.Add(new LinkedListNode<int>(5));

            linkedList.RemoveDups();

            Assert.AreEqual(linkedList.Print(), "0, 1, 2, 3, 4, 5, ");
        }

        [TestMethod]
        public void PushStack_Works()
        {
            var stack = new Stack<int>();

            stack.push(1);
            stack.push(2);
            stack.push(3);
            stack.push(4);
            stack.push(5);

            Assert.AreEqual(stack.Print(), "1,2,3,4,5");
        }

        [TestMethod]
        public void PopStack_Works()
        {
            var stack = new Stack<int>();

            stack.push(1);
            stack.push(2);
            stack.push(3);
            stack.push(4);
            stack.push(5);

            stack.pop();

            Assert.AreEqual(stack.Print(), "1,2,3,4");
        }

        [TestMethod]
        public void PopStack_OneElementWorks()
        {
            var stack = new Stack<int>();

            stack.push(1);

            stack.pop();

            Assert.AreEqual(stack.Print(), "");
        }

        [TestMethod]
        public void SeekStack_Works()
        {
            var stack = new Stack<int>();

            stack.push(1);
            stack.push(2);
            stack.push(3);
            stack.push(4);
            stack.push(5);

            var element = stack.seek(3);

            Assert.AreEqual(element, 4);
        }

        [TestMethod]
        public void MinStack_Works()
        {
            var stack = new Stack<int>();

            stack.push(0);
            stack.push(1);
            stack.push(2);
            stack.push(3);
            stack.push(4);
            stack.push(5);

            var element = stack.min();

            Assert.AreEqual(element, 0);
        }

        [TestMethod]
        public void SortStack_Works()
        {
            var stack = new Stack<int>();

            stack.push(4);
            stack.push(2);
            stack.push(5);
            stack.push(3);
            stack.push(0);
            stack.push(1);

            stack.sort();

            Assert.AreEqual(stack.Print(), "0,1,2,3,4,5");
        }

        [TestMethod]
        public void PushQueue_Works()
        {
            var queue = new Queue<int>();

            queue.push(1);
            queue.push(2);
            queue.push(3);
            queue.push(4);
            queue.push(5);

            Assert.AreEqual(queue.Print(), "1,2,3,4,5");
        }

        [TestMethod]
        public void PopQueue_Works()
        {
            var queue = new Queue<int>();

            queue.push(1);
            queue.push(2);
            queue.push(3);
            queue.push(4);
            queue.push(5);

            queue.pop();

            Assert.AreEqual(queue.Print(), "2,3,4,5");
        }

        [TestMethod]
        public void PopQueue_OneElementWorks()
        {
            var queue = new Queue<int>();

            queue.push(1);

            queue.pop();

            Assert.AreEqual(queue.Print(), "");
        }

        [TestMethod]
        public void SeekQueue_Works()
        {
            var queue = new Queue<int>();

            queue.push(1);
            queue.push(2);
            queue.push(3);
            queue.push(4);
            queue.push(5);

            var element = queue.seek(3);

            Assert.AreEqual(element, 4);
        }

        [TestMethod]
        public void Fibonacci_Works()
        {
            var result = new List<int>();

            for (var i = 0; i < 15; i++)
            {
                result.Add(Fibonacci.Calculate(i));
            }
        }

        [TestMethod]
        public void Factorial_Works()
        {
            Assert.AreEqual(Factorial.Calculate(5), 120);
        }
    }
}
