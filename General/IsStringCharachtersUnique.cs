﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace General
{
    partial class GeneralAlgorithms
    {
        private static bool IsStringCharachtersUnique(string value)
        {
            var result = true;

            for (var j = 0; j < value.Length; j++)
            {
                var c = value.Substring(j, 1);

                if (!string.IsNullOrEmpty(c) && j + 1 <= value.Length)
                {
                    if (value.Substring(j + 1).IndexOf(c) > -1)
                    {
                        result = false;
                        break;
                    }
                }
            }

            return result;
        }
    }
}
