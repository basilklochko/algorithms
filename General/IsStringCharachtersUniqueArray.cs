﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace General
{
    partial class GeneralAlgorithms
    {
        private static bool IsStringCharachtersUniqueArray(string value)
        {
            var result = true;

            var charachters = value.ToCharArray();
            var temp = new List<char>();

            charachters.ToList().ForEach(c =>
            {
                if (result)
                {
                    if (temp.IndexOf(c) == -1)
                    {
                        temp.Add(c);
                    }
                    else
                    {
                        result = false;
                    }
                }
            });

            return result;
        }
    }
}
