﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace General
{
    public class LinkedListNode<T>
    {
        public LinkedListNode<T> Next;
        public T Value;

        public LinkedListNode(T value)
        {
            Value = value;
        }

        public bool Equals(LinkedListNode<T> node)
        {
            return Value.ToString() == (node as LinkedListNode<T>).Value.ToString();
        }
    }

    public class LinkedList<T>
    {
        private LinkedListNode<T> head;
        private LinkedListNode<T> current;
        public int Count;

        public LinkedList()
        {

        }

        public void Add(LinkedListNode<T> node)
        {
            if (Count == 0)
            {
                head = node;
                current = node;
            }
            else
            {
                current.Next = node;
                current = node;
            }

            Count++;
        }

        public void Clear()
        {
            head = null;
            current = null;
            Count = 0;
        }

        public void RemoveDups()
        {
            var tmp = new List<T>();
            var i = 0;
            var del = new List<int>();
            var currentNode = head;

            while (currentNode != null)
            {
                if (tmp.Any(o => currentNode.Value.Equals(o)))
                {
                    del.Add(i);
                }
                else
                {
                    tmp.Add(currentNode.Value);
                }

                currentNode = currentNode.Next;

                i++;
            }

            del.ForEach(d =>
            {
                DeleteByIndex(d);
            });
        }

        public void DeleteByIndex(int index)
        {
            var currentNode = head;
            var i = 0;

            while (currentNode != null)
            {
                if (i == index)
                {
                    var parent = FindParent(currentNode);

                    if (parent == null)
                    {
                        head = currentNode.Next;
                    }
                    else
                    {
                        parent.Next = currentNode.Next;
                    }

                    Count--;
                    break;
                }

                i++;
                currentNode = currentNode.Next;
            }
        }

        public void DeleteIfUNique<T>(T value)
        {
            var currentNode = head;

            while (currentNode != null)
            {
                if (value.Equals(currentNode.Value))
                {
                    var parent = FindParent(currentNode);

                    if (parent == null)
                    {
                        head = currentNode.Next;
                    }
                    else
                    {
                        parent.Next = currentNode.Next;
                    }

                    Count--;
                    break;
                }

                currentNode = currentNode.Next;
            }
        }

        public void Reverse()
        {
            var newCurrent = current;
            var result = new LinkedList<T>();

            result.Add(newCurrent);

            var i = 0;

            while (i < Count)
            {
                var parent = FindParent(newCurrent);
                newCurrent.Next = parent;

                result.Add(newCurrent);
                result.current = newCurrent;

                newCurrent = parent;

                i++;
            }

            Clear();

            this.Add(result.head);
            this.current = result.current;
            this.current.Next = null;
            this.Count = i;
        }

        private LinkedListNode<T> FindParent(LinkedListNode<T> node)
        {
            var currentNode = head;

            while (currentNode != null)
            {
                if (currentNode.Next == null)
                {
                    return null;
                }
                else
                {
                    if (currentNode.Next == node)
                    {
                        return currentNode;
                    }

                    currentNode = currentNode.Next;
                }
            }

            return null;
        }

        public string Print()
        {
            var result = "";
            var currentNode = head;

            while (currentNode != null)
            {
                result += currentNode.Value.ToString() + ", ";
                currentNode = currentNode.Next;
            }

            return result;
        }
    }
}
