﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace General
{
    public class Queue<T>
    {
        // Queue FIFO
        // abc
        // push d
        // abcd
        // pop
        // bcd
        private T[] list;

        public void push(T value)
        {
            if (list == null)
            {
                list = new T[1];
            }
            else
            {
                Array.Resize<T>(ref list, list.Length + 1);
            }

            list[list.Length - 1] = value;
        }

        public void pop()
        {
            for (var i = 0; i < list.Length - 1; i++)
            {
                list[i] = list[i + 1];
            }

            Array.Resize<T>(ref list, list.Length - 1);
        }

        public T seek(int index)
        {
            return list[index];
        }

        public string Print()
        {
            return string.Join(",", list);
        }
    }
}
