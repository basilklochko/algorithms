﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace General
{
    partial class GeneralAlgorithms
    {
        private static string RemoveDups(string value)
        {
            var result = string.Empty;
           
            for (var i = 0; i < value.Length; i++)
            {
                var x = value.Substring(i, 1);

                if (!CheckResult(x, result))
                {
                    result += x;                 
                }
            }
            
            return result;
        }

        private static bool CheckResult(string letter, string value)
        {
            var result = false;

            for (var j = 0; j < value.Length; j++)
            {
                var y = value.Substring(j, 1);

                if (letter == y)
                {
                    result = true;
                }                      
            }

            return result;
        }
    }
}
