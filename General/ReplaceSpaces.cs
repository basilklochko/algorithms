﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace General
{
    public partial class GeneralAlgorithms
    {
        public static string ReplaceSpaces(string input)
        {
            var result = "";

            for (var i = 0; i < input.Length; i++)
            {
                var letter = input.Substring(i, 1);

                if (letter == " ")
                {
                    result += "%20";
                }
                else
                {
                    result += letter;
                }
            }

            return result;
        }
    }
}
