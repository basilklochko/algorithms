﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace General
{
    partial class GeneralAlgorithms
    {
        private static string ReverseString(string value)
        {
            var result = string.Empty;
            var i = value.Length;

            while (i > 0)
            {
                var c = value.Substring(i - 1, 1);
                result += c;

                i--;
            }

            return result;            
        }
    }
}
