﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace General
{
    public class Stack<T>
    {
        // Stack LIFO
        // abc
        // push d
        // abcd
        // pop
        // abc
        private T[] list;

        public void push(T value)
        {
            if (list == null)
            {
                list = new T[1];
            }
            else
            {
                Array.Resize<T>(ref list, list.Length + 1);
            }

            list[list.Length - 1] = value;
        }

        public void pop()
        {
            Array.Resize<T>(ref list, list.Length - 1);
        }

        public T seek(int index)
        {
            return list[index];
        }

        public T min()
        {
            if (!(list[0] is Int32))
            {
                throw new Exception("The stack isn't built with integers!");
            }

            var result = list[0];

            for (var i = 1; i < list.Length; i++)
            {
                if (Convert.ToInt32(list[i]) < Convert.ToInt32(result))
                {
                    result = list[i];
                }
            }

            return result;
        }

        public void sort()
        {
            if (!(list[0] is Int32))
            {
                throw new Exception("The stack isn't built with integers!");
            }

            for (var i = 0; i < list.Length; i++)
            {
                for (var j = 0; j < list.Length - 1; j++)
                {
                    if (Convert.ToInt32(list[j]) > Convert.ToInt32(list[j + 1]))
                    {
                        var temp = list[j + 1];
                        list[j + 1] = list[j];
                        list[j] = temp;
                    }
                }
            }
        }

        public string Print()
        {
            return string.Join(",", list);
        }
    }
}
