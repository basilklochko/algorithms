async function foo() {
    let promise = new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve("foo");
        }, 3000);
    });

    let response = await promise;
    console.log(response);
}

foo();
