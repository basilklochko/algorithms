// constants
const PI = 3.14;

// arrow functions
odds = evens.map(v => v + 1);

// expression Bodies
odds = evens.map(function (v) { return v + 1; });

// this

