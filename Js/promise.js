var promise = new Promise((resolve, reject) => {
    setTimeout(() => {
        resolve("foo");
    }, 3000);
});

promise.then((value) => {
    console.log(value);
});