var Employee = function (name) {
    this.name = name;
}

Employee.prototype.getName = function () {
    return this.name;
}

Employee.prototype.getNameLength = function () {
    return this.name.length;
}

var PermanentEmployee = function (annualSalary) {
    this.annualSalary = annualSalary;
}

var employee = new Employee("Mark");
PermanentEmployee.prototype = employee;

var pe = new PermanentEmployee(5000);

console.log(pe.getName());

console.log(pe instanceof Employee);
console.log(pe instanceof PermanentEmployee);

console.log(pe.getNameLength());

console.log(employee.hasOwnProperty("name"));
console.log(employee.hasOwnProperty("annualSalary"));

console.log(pe.hasOwnProperty("name"));
console.log(pe.hasOwnProperty("annualSalary"));