console.log("global: " + varX);
console.log("global: " + letX);

{
    var varX = "varX";
    let letX = "letX";

    console.log("global: " + varX);
    console.log("global: " + letX);
}

console.log("global: " + varX);
console.log("global: " + letX);
